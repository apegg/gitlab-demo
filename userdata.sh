#!/usr/bin/env bash

set -e

# This script setups the AWS EC2
# Installs docker and docker-compose to run the Gitlab-ce image

function housekeeping() {
  printf "/// \n"
  printf "/// Running housekeeping tasks\n"
  printf "/// \n"
  sleep 3

  apt-get update && \
  DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get install -y -q \
   apt-transport-https

  DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get install -y -q \
   software-properties-common \
   curl \
   git

  if [ ! -d /srv/gitlab ]; then
    mkdir /srv/gitlab
  fi

  if [ -d gitlab-demo ]; then
    rm -Rf gitlab-demo
  fi
}

function install_docker() {
  printf "/// \n"
  printf "/// Installing Docker\n"
  printf "/// \n"
  sleep 3

  curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

  add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/debian \
     $(lsb_release -cs) \
     stable"

   apt-get update && \
   DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get install -y -q \
    docker-ce \
    docker-ce-cli \
    docker-compose

}

function get_files() {
  username=' '
  password=' '

  printf "/// \n"
  printf "/// Grabbing files from git\n"
  printf "/// \n"
  sleep 3

  #git clone git@gitlab.com:apegg/gitlab-demo.git
  #git clone https://" + apegg + ":" + M3&xwXQmC$ + "@" + gitlab.com";

  git clone https://${username}:${password}@gitlab.com/apegg/gitlab-demo.git


}

function run_gitlab() {
  printf "/// \n"
  printf "/// Running docker services\n"
  printf "/// \n"
  cd /root/gitlab-demo

  docker-compose up -d
  docker logs -f gitlabdemo_web_1
}

main() {
  housekeeping
  get_files

  if hash docker; then
    printf "/// \n"
    printf "/// Docker installed\n"
    printf "/// \n"
  #  run_gitlab
  else
    install_docker
  #  run_gitlab
  fi

}

main

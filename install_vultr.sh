#!/usr/bin/env bash

function install_vultr_provider() {
  printf "///  Installing vultr provider\n"
  printf "/// \n"
  sleep 5

  cd /opt/terraform
  sudo mkdir -p terraform-provider-vultr/src/github.com/rgl/terraform-provider-vultr
  cd terraform-provider-vultr
  git clone https://github.com/rgl/terraform-provider-vultr src/github.com/rgl/terraform-provider-vultr
  export GOPATH=$PWD
  export PATH=$PWD/bin:$PATH
  hash -r # reset bash path

  go get github.com/hashicorp/terraform
  go get github.com/JamesClonk/vultr

  cd src/github.com/rgl/terraform-provider-vultr
  go build
  go test

  
  cp terraform-provider-vultr* $GOPATH/bin


  if [ ! -f ~/.terraformrc ]; then
    touch ~/.terraformrc
  fi

  cat <<EOT ~/.terraformrc
  providers {
      vultr = "$GOPATH/bin/terraform-provider-vultr"
  }
EOT
}

if hash vultr; then
  printf "///  Vultr installed\n"
  printf "/// \n"
else
  install_vultr_provider
fi


EXTERNAL_IP=66.42.92.79

docker:
	./userdata.sh

build:
	docker build -t gitlab-demo:latest .

rebuild:
	docker build --no-cache -t gitlab-demo:latest .

run:
	docker run --detach \
	--hostname gitlab.example.com \
	--publish 443:443 \
	--publish 80:80 \
	--publish 222:22 \
	--name gitlab \
	--restart always \
	--volume /srv/gitlab/config:/etc/gitlab \
	--volume /srv/gitlab/logs:/var/log/gitlab \
	--volume /srv/gitlab/data:/var/opt/gitlab \
	gitlab/gitlab-ce:latest

run_with_ports:
	docker run --detach \
	--hostname gitlab-demo.example.com \
	--publish ${EXTERNAL_IP}:443:443 \
	--publish ${EXTERNAL_IP}:80:80 \
	--publish 222:22 \
	--name gitlab \
	--restart always \
	--volume /srv/gitlab/config:/etc/gitlab \
	--volume /srv/gitlab/logs:/var/log/gitlab \
	--volume /srv/gitlab/data:/var/opt/gitlab \
	gitlab/gitlab-ce:latest

	docker logs -f gitlab

compose:
	docker-compose up -d
	docker logs -f gitlabdemo_web_1

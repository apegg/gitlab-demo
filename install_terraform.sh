#!/usr/bin/env bash

set -e

TVERSION='0.11.11'


get_terraform() {
 if [ ! -d /opt/terraform ]; then 
  sudo mkdir /opt/terraform
  fi 
  cd /opt/terraform
  wget https://releases.hashicorp.com/terraform/${TVERSION}/terraform_${TVERSION}_linux_amd64.zip
  unzip terraform_${TVERSION}_linux_amd64.zip
  sudo ln -s terraform /usr/local/bin/terraform

  }

  if [ ! -f /opt/terraform/terraform_${TVERSION}_linux_amd64.zip ]; then
	    get_terraform

else
      echo "Terraform installed"
        exit 0
fi

